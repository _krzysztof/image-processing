﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.IO;
using System.Windows.Controls;

namespace PrzetwarzanieObrazow
{
    // Do usunięcia w finalnej wersji - skaluje okno wg DPI systemu
    public class DpiDecorator : Decorator
    {
        public DpiDecorator()
        {
            this.Loaded += (s, e) =>
            {
                Matrix m = PresentationSource.FromVisual(this).CompositionTarget.TransformToDevice;
                ScaleTransform dpiTransform = new ScaleTransform(1 / m.M11, 1 / m.M22);
                if (dpiTransform.CanFreeze)
                    dpiTransform.Freeze();
                this.LayoutTransform = dpiTransform;
            };
        }
    }

    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Funkcje użyte przy zaznaczaniu fragmentu obrazu
        enum SelectionMethod {
            Cut,
            CopyAndPuste,
            Point,
            Line
        };

        SelectionMethod selectionMethod;

        bool inSelectionMode = false;
        bool startedSelection = false;
        System.Windows.Point selectionStartPoint = new System.Windows.Point();
        System.Windows.Point selectionEndPoint = new System.Windows.Point();
        
        private void SelectionCut_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            selectionMethod = SelectionMethod.Cut;
            inSelectionMode = true;
        }

        private void SelectionCopyAndPaste_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            selectionMethod = SelectionMethod.CopyAndPuste;
            inSelectionMode = true;
        }

        private void PointCopyAndPaste_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            selectionMethod = SelectionMethod.Point;
        }

        private void LineDraw_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            selectionMethod = SelectionMethod.Line;
            inSelectionMode = true;
        }

        private void CanvasA_MouseDown(object sender, MouseEventArgs e)
        {
            if (selectionMethod == SelectionMethod.Point)
            {
                copyAndPasteX3.Text = Convert.ToString(
                    Convert.ToInt32(e.GetPosition(canvasA).X - (canvasA.Width - imageA.Width) / 2)
                );
                copyAndPasteY3.Text = Convert.ToString(
                    Convert.ToInt32(e.GetPosition(canvasA).Y - (canvasA.Height - imageA.Height) / 2)
                );
            }
            else
            {
                if (inSelectionMode)
                {
                    startedSelection = true;
                    selectionStartPoint.X = e.GetPosition(canvasA).X;
                    selectionStartPoint.Y = e.GetPosition(canvasA).Y;
                    selectionEndPoint.X = e.GetPosition(canvasA).X;
                    selectionEndPoint.Y = e.GetPosition(canvasA).Y;
                }
            }

            if (imageA.IsOK())
                imageA.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);
        }

        private void CanvasA_MouseUp(object sender, MouseEventArgs e)
        {
            if (startedSelection)
            {
                startedSelection = false;
                inSelectionMode = false;

                if (selectionMethod == SelectionMethod.Cut)
                {
                    CutX1.Text = Convert.ToString(
                        Convert.ToInt32(selectionStartPoint.X - (canvasA.Width - imageA.Width) / 2)
                    );
                    CutY1.Text = Convert.ToString(
                        Convert.ToInt32(selectionStartPoint.Y - (canvasA.Height - imageA.Height) / 2)
                    );
                    CutX2.Text = Convert.ToString(
                        Convert.ToInt32(selectionEndPoint.X - (canvasA.Width - imageA.Width) / 2)
                    );
                    CutY2.Text = Convert.ToString(
                        Convert.ToInt32(selectionEndPoint.Y - (canvasA.Height - imageA.Height) / 2)
                    );
                }
                else if (selectionMethod == SelectionMethod.Line)
                {
                    lineSymetryX1.Text = Convert.ToString(
                        Convert.ToInt32(selectionStartPoint.X - (canvasA.Width - imageA.Width) / 2)
                    );
                    lineSymetryY1.Text = Convert.ToString(
                        Convert.ToInt32(selectionStartPoint.Y - (canvasA.Height - imageA.Height) / 2)
                    );
                    lineSymetryX2.Text = Convert.ToString(
                        Convert.ToInt32(selectionEndPoint.X - (canvasA.Width - imageA.Width) / 2)
                    );
                    lineSymetryY2.Text = Convert.ToString(
                        Convert.ToInt32(selectionEndPoint.Y - (canvasA.Height - imageA.Height) / 2)
                    );
                }
                else
                {
                    copyAndPasteX1.Text = Convert.ToString(
                        Convert.ToInt32(selectionStartPoint.X - (canvasA.Width - imageA.Width) / 2)
                    );
                    copyAndPasteY1.Text = Convert.ToString(
                        Convert.ToInt32(selectionStartPoint.Y - (canvasA.Height - imageA.Height) / 2)
                    );
                    copyAndPasteX2.Text = Convert.ToString(
                        Convert.ToInt32(selectionEndPoint.X - (canvasA.Width - imageA.Width) / 2)
                    );
                    copyAndPasteY2.Text = Convert.ToString(
                        Convert.ToInt32(selectionEndPoint.Y - (canvasA.Height - imageA.Height) / 2)
                    );
                }

                canvasA.Children.Clear();
                if (imageA.IsOK())
                    imageA.DrawInCanvas(canvasA);
            }
        }

        private void CanvasA_MouseMove(object sender, MouseEventArgs e)
        {
            if (startedSelection)
            {
                if (selectionMethod == SelectionMethod.Line)
                {
                    selectionEndPoint.X = e.GetPosition(canvasA).X;
                    selectionEndPoint.Y = e.GetPosition(canvasA).Y;
                    System.Windows.Shapes.Line line = new System.Windows.Shapes.Line();

                    line.X1 = selectionStartPoint.X;
                    line.Y1 = selectionStartPoint.Y;
                    line.X2 = selectionEndPoint.X;
                    line.Y2 = selectionEndPoint.Y;
                    
                    line.Stroke = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 255, 0));
                    line.StrokeThickness = 5;
                    
                    canvasA.Children.Clear();
                    if (imageA.IsOK())
                        imageA.DrawInCanvas(canvasA);
                    canvasA.Children.Add(line);

                    foreach (UIElement el in canvasA.Children)
                        el.MouseLeftButtonUp += CanvasA_MouseUp;
                }
                else
                {
                    selectionEndPoint.X = e.GetPosition(canvasA).X;
                    selectionEndPoint.Y = e.GetPosition(canvasA).Y;

                    System.Windows.Shapes.Rectangle rectangle = new System.Windows.Shapes.Rectangle();

                    rectangle.Width = Math.Abs(selectionEndPoint.X - selectionStartPoint.X);
                    rectangle.Height = Math.Abs(selectionEndPoint.Y - selectionStartPoint.Y);
                    rectangle.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 255, 0, 0));

                    Canvas.SetLeft(rectangle, Math.Min(selectionEndPoint.X, selectionStartPoint.X));
                    Canvas.SetTop(rectangle, Math.Min(selectionEndPoint.Y, selectionStartPoint.Y));

                    canvasA.Children.Clear();
                    if (imageA.IsOK())
                        imageA.DrawInCanvas(canvasA);
                    canvasA.Children.Add(rectangle);

                    foreach (UIElement el in canvasA.Children)
                        el.MouseLeftButtonUp += CanvasA_MouseUp;
                }
                
            }
        }

        private void CanvasB_MouseDown(object sender, MouseEventArgs e)
        {
            if (imageB.IsOK())
                imageB.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);
        }

        private void CanvasC_MouseDown(object sender, MouseEventArgs e)
        {
            if (resultImage.IsOK())
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);
        }

        //Nasze magiczne obrazy 
        MatrixImage imageA, imageB, resultImage;

        void CenterScrollViewer(ScrollViewer sv, Canvas c)
        {
            sv.ScrollToHorizontalOffset(c.ActualWidth / 2 - sv.ActualWidth / 2);
            sv.ScrollToVerticalOffset(c.ActualHeight / 2 - sv.ActualHeight / 2);
        }

        //Przyciski uruchamiające operacje na obrazach
        
        //ZADANIE 1
        //Ujednolicanie geometryczne
        private void GeometricUniform_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != imageB.Channels)
            {
                MessageBox.Show("Obrazy muszą mieć ten sam profil barw.");
                return;
            }

            ImageOperations.GeometricUniform(ref imageA, ref imageB);
            imageA.DrawInCanvas(canvasA);
            imageB.DrawInCanvas(canvasB);

            imageA.Histogram = ImageOperations.CalculateHistogram(imageA);
            imageB.Histogram = ImageOperations.CalculateHistogram(imageB);
            imageA.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasAScrollViewer, canvasA);
            CenterScrollViewer(canvasBScrollViewer, canvasB);
        }

        //Ujednolicanie w rastrze
        private void RasterUniform_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != imageB.Channels)
            {
                MessageBox.Show("Obrazy muszą mieć ten sam profil barw.");
                return;
            }

            ImageOperations.RasterUniform(ref imageA, ref imageB);
            imageA.DrawInCanvas(canvasA);
            imageB.DrawInCanvas(canvasB);

            imageA.Histogram = ImageOperations.CalculateHistogram(imageA);
            imageB.Histogram = ImageOperations.CalculateHistogram(imageB);
            imageA.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasAScrollViewer, canvasA);
            CenterScrollViewer(canvasBScrollViewer, canvasB);
        }

        //ZADANIE 2
        //Dodawanie 2 obrazów
        private void Addition_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != imageB.Channels)
            {
                MessageBox.Show("Obrazy muszą mieć ten sam profil barw.");
                return;
            }

            resultImage = ImageOperations.Addition(imageA, imageB);
            resultImage.DrawInCanvas(canvasResult, false);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);

            //Normalizacja
            MessageBox.Show("Teraz nastąpi normalizacja");
            resultImage = ImageOperations.Normalize(resultImage);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Dodawanie obrazu ze stałą
        private void AdditionWithConstant_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int constant = Convert.ToInt32(additionWithConstant.Text);

                resultImage = ImageOperations.Addition(imageA, constant);
                resultImage.DrawInCanvas(canvasResult, false);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);

                //Normalizacja
                MessageBox.Show("Teraz nastąpi normalizacja");
                resultImage = ImageOperations.Normalize(resultImage);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę.");
            }
        }

        //Mnożenie 2 obrazów
        private void Multiplication_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != imageB.Channels)
            {
                MessageBox.Show("Obrazy muszą mieć ten sam profil barw.");
                return;
            }

            resultImage = ImageOperations.Multiplication(imageA, imageB);
            resultImage.DrawInCanvas(canvasResult, false);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);

            //Normalizacja
            MessageBox.Show("Teraz nastąpi normalizacja");
            resultImage = ImageOperations.Normalize(resultImage);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Mnożenie obrazu ze stałą
        private void MultiplicationWithConstant_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double constant = Convert.ToDouble(multiplicationWithConstant.Text);

                if (constant < 0)
                    throw new Exception();

                resultImage = ImageOperations.Multiplication(imageA, constant);
                resultImage.DrawInCanvas(canvasResult, false);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);

                //Normalizacja
                MessageBox.Show("Teraz nastąpi normalizacja");
                resultImage = ImageOperations.Normalize(resultImage);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //Miesanie 2 obrazów ze współczynnikiem
        private void Mixing_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != imageB.Channels)
            {
                MessageBox.Show("Obrazy muszą mieć ten sam profil barw.");
                return;
            }

            try
            {
                double alpha = Convert.ToDouble(mixing.Text);
                MessageBox.Show(alpha.ToString());

                if (alpha < 0.0 || alpha > 1.0)
                    throw new Exception();

                resultImage = ImageOperations.Mixing(imageA, imageB, alpha);
                resultImage.DrawInCanvas(canvasResult, false);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);

                //Normalizacja
                MessageBox.Show("Teraz nastąpi normalizacja");
                resultImage = ImageOperations.Normalize(resultImage);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę w przedziale [0, 1].");
            }
        }

        //Potęgowanie obrazu
        private void Exponentiation_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double param = Convert.ToDouble(exponentiation.Text);

                if (param < 0)
                    throw new Exception();

                resultImage = ImageOperations.Exponentiation(imageA, param);
                resultImage.DrawInCanvas(canvasResult, false);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);

                //Normalizacja
                MessageBox.Show("Teraz nastąpi normalizacja");
                resultImage = ImageOperations.Normalize(resultImage);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //Dzielenie 2 obrazów Division
        private void Division_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != imageB.Channels)
            {
                MessageBox.Show("Obrazy muszą mieć ten sam profil barw.");
                return;
            }

            resultImage = ImageOperations.Division(imageA, imageB);
            resultImage.DrawInCanvas(canvasResult, false);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);

            //Normalizacja
            MessageBox.Show("Teraz nastąpi normalizacja");
            resultImage = ImageOperations.Normalize(resultImage);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Dzielenie obrazu ze stałą
        private void DivisionWithConstant_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double constant = Convert.ToDouble(divisionWithConstant.Text);

                if (constant <= 0)
                    throw new Exception();

                resultImage = ImageOperations.Division(imageA, constant);
                resultImage.DrawInCanvas(canvasResult, false);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);

                //Normalizacja
                MessageBox.Show("Teraz nastąpi normalizacja");
                resultImage = ImageOperations.Normalize(resultImage);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //Logarytmowanie obrazu 
        private void Logarithm_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double multiplication = Convert.ToDouble(logarithm.Text);

                if (multiplication <= 0)
                    throw new Exception();

                resultImage = ImageOperations.Logarithm(imageA, multiplication);
                resultImage.DrawInCanvas(canvasResult, false);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);

                //Normalizacja
                MessageBox.Show("Teraz nastąpi normalizacja");
                resultImage = ImageOperations.Normalize(resultImage);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        private void RootExtraction_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double param = Convert.ToDouble(rootExtraction.Text);

                if (param < 0)
                    throw new Exception();

                resultImage = ImageOperations.RootExtraction(imageA, param);
                resultImage.DrawInCanvas(canvasResult, false);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);

                //Normalizacja
                MessageBox.Show("Teraz nastąpi normalizacja");
                resultImage = ImageOperations.Normalize(resultImage);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        // ** ZADANIA 3
        //Przesunięcie o wektor
        private void Shifting_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int x = Convert.ToInt32(shiftingX.Text);
                int y = Convert.ToInt32(shiftingY.Text);

                resultImage = ImageOperations.Shifting(imageA, x, y);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Proszę wpisać liczby.");
            }
        }

        //Jednorodne skalowanie obrazu
        private void HomogeneousGraduation_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double scale = Convert.ToDouble(homogeneousGraduation.Text);

                if (scale <= 0.0)
                    throw new Exception();

                resultImage = ImageOperations.Graduation(imageA, scale, scale);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //Niejednokrotne skalowanie obrazu
        private void Graduation_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }
            try
            {
                double x = Convert.ToDouble(graduationX.Text);
                double y = Convert.ToDouble(graduationY.Text);

                if (x <= 0.0 || y <= 0.0)
                    throw new Exception();

                resultImage = ImageOperations.Graduation(imageA, x, y);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Proszę wpisać liczby dodatnie.");
            }
        }

        //Obracanie obrazu
        private void Rotation_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double angle = Convert.ToDouble(rotation.Text);

                resultImage = ImageOperations.Rotation(imageA, angle);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Proszę wpisać liczbę.");
            }
        }

        //Symetria względem X
        private void SymetryX_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.SymetryX(imageA);
            resultImage.DrawInCanvas(canvasResult);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Symetria względem Y
        private void SymetryY_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.SymetryY(imageA);
            resultImage.DrawInCanvas(canvasResult);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Symetria względem prostej
        private void LineSymetry_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int X1 = Convert.ToInt32(lineSymetryX1.Text);
                int Y1 = Convert.ToInt32(lineSymetryY1.Text);
                int X2 = Convert.ToInt32(lineSymetryX2.Text);
                int Y2 = Convert.ToInt32(lineSymetryY2.Text);

                resultImage = ImageOperations.LineSymetry(imageA, X1, Y1, X2, Y2);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczby bądź zaznaczyć obszar obrazu.");
            }
        }

        //Wycinanie fragmentu obrazu
        private void Cut_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int X1 = Convert.ToInt32(CutX1.Text);
                int Y1 = Convert.ToInt32(CutY1.Text);
                int X2 = Convert.ToInt32(CutX2.Text);
                int Y2 = Convert.ToInt32(CutY2.Text);

                resultImage = ImageOperations.Cut(imageA, X1, Y1, X2, Y2);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczby bądź zaznaczyć obszar obrazu.");
            }
        }

        //Wycinanie fragmentu obrazu
        private void CopyAndPaste_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int X1 = Convert.ToInt32(copyAndPasteX1.Text);
                int Y1 = Convert.ToInt32(copyAndPasteY1.Text);
                int X2 = Convert.ToInt32(copyAndPasteX2.Text);
                int Y2 = Convert.ToInt32(copyAndPasteY2.Text);
                int X3 = Convert.ToInt32(copyAndPasteX3.Text);
                int Y3 = Convert.ToInt32(copyAndPasteY3.Text);

                resultImage = ImageOperations.CopyAndPaste(imageA, X1, Y1, X2, Y2, X3, Y3);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczby bądź zaznaczyć obszar obrazu i punkt wklejenia.");
            }
        }

        //ZADANIE 5:
        //Przesuwanie histogramu
        private void MoveHistogram_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int constant = Convert.ToInt32(moveHistogram.Text);

                resultImage = ImageOperations.MoveHistogram(imageA, constant);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę.");
            }
        }

        //Pionowe skalowanie histogramu 
        private void VerticallyScaleHistogram_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                double constant = Convert.ToDouble(verticallyScaleHistogram.Text);

                if (constant <= 0.0)
                    throw new Exception();

                resultImage = ImageOperations.VerticallyScaleHistogram(imageA, constant);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //Poziome skalowanie histogramu 
        private void HorizontallyScaleHistogram_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int constant = Convert.ToInt32(horizontallyScaleHistogram.Text);

                if (constant <= 0)
                    throw new Exception();

                resultImage = ImageOperations.HorizontallyScaleHistogram(imageA, constant);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //Progowanie lokalne 
        private void LocalThresholding_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int window = Convert.ToInt32(localThresholding.Text);

                resultImage = ImageOperations.LocalThresholding(imageA, window);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //Progowanie globalne 
        private void GlobalThresholding_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int constant = Convert.ToInt32(globalThresholding.Text);

                resultImage = ImageOperations.GlobalThresholding(imageA, constant);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę.");
            }
        }

        //Progowanie wieloprogowe 
        private void MultiThresholding_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            try
            {
                int count = Convert.ToInt32(multiThresholding.Text);

                resultImage = ImageOperations.MultiThresholding(imageA, count);
                resultImage.DrawInCanvas(canvasResult);

                resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
                resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

                CenterScrollViewer(canvasResultScrollViewer, canvasResult);
            }
            catch (Exception)
            {
                MessageBox.Show("Proszę wpisać liczbę dodatnią.");
            }
        }

        //ZADANIE 7: 
        //Erozia obrazów binarnych 
        private void ErosionBinary_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Erosion(imageA, imageB, true);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Nakładanie obrazów binarnych
        private void DilationBinary_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Dilation(imageA, imageB, true);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Otwarcie obrazów binarnych
        private void OpeningBinary_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Opening(imageA, imageB, true);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Zamknięcie obrazów binarnych
        private void ClosingBinary_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Closing(imageA, imageB, true);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //ZADANIE 8:
        //Erozia obrazów szarych 
        private void Erosion_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Erosion(imageA, imageB);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Nakładanie obrazów szarych
        private void Dilation_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Dilation(imageA, imageB);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Otwarcie obrazów szarych
        private void Opening_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Opening(imageA, imageB);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Zamknięcie obrazów szarych
        private void Closing_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK() || !imageB.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać oba obrazy.");
                return;
            }

            if (imageA.Channels != 1 || imageB.Channels != 1)
            {
                MessageBox.Show("Należy wczytać obrazy ze skalą szarości.");
                return;
            }

            resultImage = ImageOperations.Closing(imageA, imageB);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //ZADANIE 9
        //Filtr dolnoprzpustowy prostokątny 3x3
        private void LowPassFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }
            
            resultImage = ImageOperations.LowPassFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr dolnoprzpustowy piramidalny 5x5
        private void LowPassFilterPyramidal_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.LowPassFilterPyramidal(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr dolnoprzpustowy Gaussa 5x5
        private void LowPassFilterGauss_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.LowPassFilterGauss(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr Robertsa
        private void RobertsFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.RobertsFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr Kirscha
        private void KirschFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.KirschFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr Prewitta
        private void PrewittFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.PrewittFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr Sobela
        private void SobelFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.SobelFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Kompasowy filtr Prewitta 
        private void CompassPrewittilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.CompassPrewittilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr medianowy 
        private void MedianFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.MedianFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr medianowy 
        private void MinFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.MinFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Filtr maksymalny 
        private void MaxFilter_Click(object sender, RoutedEventArgs e)
        {
            if (!imageA.IsOK())
            {
                MessageBox.Show("Należy najpierw wczytać obraz A.");
                return;
            }

            resultImage = ImageOperations.MaxFilter(imageA);
            resultImage.DrawInCanvas(canvasResult);

            resultImage.Histogram = ImageOperations.CalculateHistogram(resultImage);
            resultImage.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);

            CenterScrollViewer(canvasResultScrollViewer, canvasResult);
        }

        //Checkboxy sterujące wyświetlaniem obrazów A, B i wynikowego
        private void imageCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (canvasGrid == null)
                return;

            CheckBox cb = sender as CheckBox;

            switch (cb.Name)
            {
                case "imageACheckBox":
                    canvasGrid.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star);
                    CenterScrollViewer(canvasAScrollViewer, canvasA);
                    CenterScrollViewer(canvasBScrollViewer, canvasB);
                    CenterScrollViewer(canvasResultScrollViewer, canvasResult);
                    break;
                case "imageBCheckBox":
                    canvasGrid.ColumnDefinitions[1].Width = new GridLength(1, GridUnitType.Star);
                    CenterScrollViewer(canvasAScrollViewer, canvasA);
                    CenterScrollViewer(canvasBScrollViewer, canvasB);
                    CenterScrollViewer(canvasResultScrollViewer, canvasResult);
                    break;
                case "resultImageCheckBox":
                    canvasGrid.ColumnDefinitions[2].Width = new GridLength(1, GridUnitType.Star);
                    CenterScrollViewer(canvasAScrollViewer, canvasA);
                    CenterScrollViewer(canvasBScrollViewer, canvasB);
                    CenterScrollViewer(canvasResultScrollViewer, canvasResult);
                    break;
            }
        }

        //j.w.
        private void imageCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (canvasGrid == null)
                return;

            CheckBox cb = sender as CheckBox;

            switch (cb.Name)
            {
                case "imageACheckBox":
                    canvasGrid.ColumnDefinitions[0].Width = new GridLength(0);
                    CenterScrollViewer(canvasBScrollViewer, canvasB);
                    CenterScrollViewer(canvasResultScrollViewer, canvasResult);
                    break;
                case "imageBCheckBox":
                    canvasGrid.ColumnDefinitions[1].Width = new GridLength(0);
                    CenterScrollViewer(canvasAScrollViewer, canvasA);
                    CenterScrollViewer(canvasResultScrollViewer, canvasResult);
                    break;
                case "resultImageCheckBox":
                    canvasGrid.ColumnDefinitions[2].Width = new GridLength(0);
                    CenterScrollViewer(canvasAScrollViewer, canvasA);
                    CenterScrollViewer(canvasBScrollViewer, canvasB);
                    break;
            }
        }

        //Checkbox sterujący wyświetlaniem informacji o obrazie
        private void imageInfoCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            rightGrid.RowDefinitions[2].Height = new GridLength(250);
        }

        //j.w.
        private void imageInfoCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            rightGrid.RowDefinitions[2].Height = new GridLength(0);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (imageA.IsOK())
                imageA.DrawHistogramInCanvas(imageData_HistogramR, imageData_HistogramG, imageData_HistogramB);
        }

        public MainWindow()
        {
            InitializeComponent();

            //Tworzymy nowe obiekty obrazów
            imageA = new MatrixImage();
            imageB = new MatrixImage();
            resultImage = new MatrixImage();
        }

        //Przyciski ładowania obrazów z pliku
        private void TextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Grid grid = sender as Grid;
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Filter = "Pliki JPG (*.jpg)|*.jpg";

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (grid.Name == "imageALoadBUtton")
                {
                    canvasA.Children.Clear();
                    imageA.FromFile(dialog.FileName);

                    imageA.DrawInCanvas(canvasA);

                    canvasAScrollViewer.ScrollToHorizontalOffset(canvasA.ActualWidth / 2 - canvasAScrollViewer.ActualWidth / 2);
                    canvasAScrollViewer.ScrollToVerticalOffset(canvasA.ActualHeight / 2 - canvasAScrollViewer.ActualHeight / 2);

                    //Uzupełniamy informację o w czytanym obrazie 
                    imageA.ShowInfo(
                        imageData_Width, imageData_Height, imageData_Channels, imageData_DPI,
                        imageData_HistogramR, imageData_HistogramG, imageData_HistogramB
                    );
                }
                else
                {
                    canvasB.Children.Clear();
                    imageB.FromFile(dialog.FileName);
                    imageB.DrawInCanvas(canvasB);

                    canvasBScrollViewer.ScrollToHorizontalOffset(canvasB.ActualWidth / 2 - canvasBScrollViewer.ActualWidth / 2);
                    canvasBScrollViewer.ScrollToVerticalOffset(canvasB.ActualHeight / 2 - canvasBScrollViewer.ActualHeight / 2);
                }
            }
        }
    }
}
