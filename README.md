**Authors**:  
Mariusz Niedźiółka   
Krzysztof Mika  
  
  
**Image processing** is a complete application that allows to perform various operations on images, written in C#.
It comes with GUI to make the app easier to use.  
Application allows to operations such as:
* geometrical unification of images
* arithmetic operations on images
* geometric operations on images
* operations on the histogram of images
* morphological operations on images
* linear and non-linear filtering