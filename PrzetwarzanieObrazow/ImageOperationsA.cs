/*
 * Operacje z punktów 1-4
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Drawing;

namespace PrzetwarzanieObrazow
{
    public static partial class ImageOperations
    {
        /*
         * Funkcja normalizuje obraz
         */
        public static MatrixImage Normalize(MatrixImage image)
        {
            MatrixImage resultImage = new MatrixImage(image);

            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < image.Height; ++j)
                    for (int k = 0; k < image.Width; ++k)
                    {
                        if (resultImage.ImageMatrix[i][j, k] < 0)
                            resultImage.ImageMatrix[i][j, k] = 0;
                        if (resultImage.ImageMatrix[i][j, k] > 255)
                            resultImage.ImageMatrix[i][j, k] = 255;
                    }

            return resultImage;
        }


        /*
         * Funkcja sprawdza czy obrazy są z tej samej skali barw.
         * 
         * @params: MatrixImage imageA, imageB - wczytane obrazy
         * @return: bool
         */
        public static bool TheSameBitCount(MatrixImage imageA, MatrixImage imageB)
        {
            return imageA.ImageMatrix.Length == imageB.ImageMatrix.Length;
        }

        /*
         * Funkcja sprawdza czy obrazy mają taki sam wymiar.
         * 
         * @params: MatrixImage imageA, imageB - wczytane obrazy
         * @return: bool
         */
        public static bool TheSameSize(MatrixImage imageA, MatrixImage imageB)
        {
            return imageA.ImageMatrix[0].GetLength(0) == imageB.ImageMatrix[0].GetLength(0) &&
                imageA.ImageMatrix[0].GetLength(1) == imageB.ImageMatrix[0].GetLength(1);
        }

        /*
         * Funkcja interpolująca obraz
         */
        public static MatrixImage Interpolate(MatrixImage image)
        {
            MatrixImage resultImage = new MatrixImage(image);

            for (int i = 0; i < resultImage.Channels; ++i)
                for (int j = 0; j < resultImage.Height; ++j)
                    for (int k = 0; k < resultImage.Width; ++k)
                        if (resultImage.ImageMatrix[i][j, k] == 0)
                        {
                            int neighbours = 0;
                            int sum = 0;

                            if (j > 0) { sum += resultImage.ImageMatrix[i][j - 1, k]; neighbours++; }
                            if (j > 0 && k < image.Width - 1) { sum += resultImage.ImageMatrix[i][j - 1, k + 1]; neighbours++; }
                            if (k < image.Width - 1) { sum += resultImage.ImageMatrix[i][j, k + 1]; neighbours++; }
                            if (j < image.Height - 1 && k < image.Width - 1) { sum += resultImage.ImageMatrix[i][j + 1, k + 1]; neighbours++; }
                            if (j < image.Height - 1) { sum += resultImage.ImageMatrix[i][j + 1, k]; neighbours++; }
                            if (j < image.Height - 1 && k > 0) { sum += resultImage.ImageMatrix[i][j + 1, k - 1]; neighbours++; }
                            if (k > 0) { sum += resultImage.ImageMatrix[i][j, k - 1]; neighbours++; }
                            if (j > 0 && k > 0) { sum += resultImage.ImageMatrix[i][j - 1, k - 1]; neighbours++; }

                            resultImage.ImageMatrix[i][j, k] = (byte)(sum / neighbours);
                        }

            return resultImage;
        }

        /*
         * Funkcja geometrycznie ujednolicająca dwa obrazy.
         * 
         * @params: MatrixImage imageA, imageB - wczytane obrazy
         */
        public static void GeometricUniform(ref MatrixImage imageA, ref MatrixImage imageB)
        {
            MatrixImage wider, higher, narrower, lower;

            wider = imageA.Width > imageB.Width ? imageA : imageB;
            higher = imageA.Height > imageB.Height ? imageA : imageB;

            narrower = wider == imageA ? imageB : imageA;
            lower = higher == imageA ? imageB : imageA;

            if (wider == imageA && higher == imageA)
                imageB = Shifting(imageB, -(imageA.Width - imageB.Width), (imageA.Height - imageB.Height));
            else if (wider == imageA && higher == imageB)
            {
                imageB = Shifting(imageB, -(imageA.Width - imageB.Width), 0);
                imageA = Shifting(imageA, 0, (imageB.Height - imageA.Height));
            }
            else if (wider == imageB && higher == imageA)
            {
                imageA = Shifting(imageA, -(imageB.Width - imageA.Width), 0);
                imageB = Shifting(imageB, 0, (imageA.Height - imageB.Height));
            }
            else
                imageA = Shifting(imageA, -(imageB.Width - imageA.Width), (imageB.Height - imageA.Height));
        }

        /*
         * Funkcja ujednolicająca dwa obrazy w rastrze.
         * 
         * @params: MatrixImage imageA, imageB - wczytane obrazy
         */
        public static void RasterUniform(ref MatrixImage imageA, ref MatrixImage imageB)
        {
            MatrixImage wider, higher, narrower, lower;

            wider = imageA.Width > imageB.Width ? imageA : imageB;
            higher = imageA.Height > imageB.Height ? imageA : imageB;

            narrower = wider == imageA ? imageB : imageA;
            lower = higher == imageA ? imageB : imageA;

            double scaleX = (double)wider.Width / (double)narrower.Width;
            double scaleY = (double)higher.Height / (double)lower.Height;

            if (wider == imageA && higher == imageA)
                imageB = Graduation(imageB, scaleX, scaleY);
            else if (wider == imageA && higher == imageB)
            {
                imageB = Graduation(imageB, scaleX, 1);
                imageA = Graduation(imageA, 1, scaleY);
            }
            else if (wider == imageB && higher == imageA)
            {
                imageA = Graduation(imageA, scaleX, 1);
                imageB = Graduation(imageB, 1, scaleY);
            }
            else
                imageA = Graduation(imageA, scaleX, scaleY);

            if (imageA.Width != imageB.Width || imageA.Height != imageB.Height)
                GeometricUniform(ref imageA, ref imageB);
        }
        //



        //

        //----------------------ZADANIE 2 & 3----------------------//

        /*
         * Funkcja dodająca do siebie 2 obrazy.
         * 
         * @params: MatrixImage imageA, imageB - wczytane obrazy
         */
        public static MatrixImage Addition(MatrixImage imageA, MatrixImage imageB)
        {
            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            

            //Sprawdzenie czy obrazy mają taką samą skalę barw.
            if (!TheSameBitCount(imageA, imageB))
                return null;

            //Jeśli obrazy mają różne wymiary - ujednolicamy
            if (!TheSameSize(imageA, imageB))
                RasterUniform(ref imageA, ref imageB);

            int maxValue = 0;

            MatrixImage resultImage = new MatrixImage(imageA);

            //Sprawdzamy czy wszystkie sumy będą <= 255
            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)(imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k]);

                        if (maxValue < imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k])
                            maxValue = imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k];
                    }

            if (maxValue <= 255)
            {
                for (int i = 0; i < imageA.Channels; ++i)
                    for (int j = 0; j < imageA.Height; ++j)
                        for (int k = 0; k < imageA.Width; ++k)
                            resultImage.ImageMatrix[i][j, k] = (byte)(imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k]);
            }
            else
            {
                double proportion = 255.0 / maxValue;
                for (int i = 0; i < imageA.Channels; ++i)
                    for (int j = 0; j < imageA.Height; ++j)
                        for (int k = 0; k < imageA.Width; ++k)
                            resultImage.ImageMatrix[i][j, k] = (byte)((imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k]) * proportion);
            }

            return resultImage;
        }

        /*
         * Funkcja dodająca stałą do obrazu.
         * 
         * @params: MatrixImage imageA - wczytany obraz, int constant - stała
         */
        public static MatrixImage Addition(MatrixImage imageA, int constant)
        {
            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            MatrixImage resultImage = new MatrixImage(imageA);
            int foo = 0;

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        foo = imageA.ImageMatrix[i][j, k] + constant;
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)foo;
                        resultImage.ImageMatrix[i][j, k] = (byte)Math.Min(255, Math.Max(0, foo));
                    }

            return resultImage;
        }

        /*
         * Funkcja mnożąca 2 obrazy przez siebie.
         * 
         * @params: MatrixImage imageA, imageB - wczytane obrazy
         */
        public static MatrixImage Multiplication(MatrixImage imageA, MatrixImage imageB)
        {
            //Sprawdzenie czy obrazy mają taką samą skalę barw.
            if (!TheSameBitCount(imageA, imageB))
                return null;

            //Jeśli obrazy mają różne wymiary - ujednolicamy
            if (!TheSameSize(imageA, imageB))
                RasterUniform(ref imageA, ref imageB);

            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            MatrixImage resultImage = new MatrixImage(imageA);

            //Właściwe mnozenie: i - liczba kanałów, j - szerokość, k - wysokość
            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)(imageA.ImageMatrix[i][j, k] * imageB.ImageMatrix[i][j, k] / 255.0);
                        resultImage.ImageMatrix[i][j, k] = (byte)Math.Min(255, Math.Max(0, (imageA.ImageMatrix[i][j, k] * imageB.ImageMatrix[i][j, k] / 255.0)));
                    }

            return resultImage;
        }

        /*
         * Funkcja mnożąca obraz przez stałą.
         * 
         * @params: MatrixImage imageA - wczytany obraz, double constant - stała
         */
        public static MatrixImage Multiplication(MatrixImage imageA, double constant)
        {
            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            MatrixImage resultImage = new MatrixImage(imageA);

            int foo = 0;

            //Właściwe mnozenie: i - liczba kanałów, j - szerokość, k - wysokość
            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        foo = (int)Math.Round(imageA.ImageMatrix[i][j, k] * constant);
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)foo;
                        resultImage.ImageMatrix[i][j, k] = (byte)Math.Min(255, Math.Max(0, foo));
                    }
                       

            return resultImage;
        }

        /*
         * Funkcja mieszająca 2 obrazy.
         * 
         * @params: MatrixImage imageA, imageB - wczytane obrazy, double alpha - wspolczynnik mieszania z przedzialu [0,1]
         */
        public static MatrixImage Mixing(MatrixImage imageA, MatrixImage imageB, double alpha)
        {
            //Sprawdzenie czy obrazy mają taką samą skalę barw.
            if (!TheSameBitCount(imageA, imageB))
                return null;

            //Jeśli obrazy mają różne wymiary - ujednolicamy
            if (!TheSameSize(imageA, imageB))
                RasterUniform(ref imageA, ref imageB);

            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            MatrixImage resultImage = new MatrixImage(imageA);

            //Właściwe mnozenie: i - liczba kanałów, j - szerokość, k - wysokość
            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        resultImage.ImageMatrix[i][j, k] = (byte)(alpha * imageA.ImageMatrix[i][j, k] + (1 - alpha) * imageB.ImageMatrix[i][j, k]);
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)(alpha * imageA.ImageMatrix[i][j, k] + (1 - alpha) * imageB.ImageMatrix[i][j, k]);
                    }

            return resultImage;
        }


        /*
        * Funkcja potęgująca obraz przez parametr.
        * 
        * @params: MatrixImage imageA- wczytany obraz, double param - parametr
        */
        public static MatrixImage Exponentiation(MatrixImage imageA, double param)
        {
            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            MatrixImage resultImage = new MatrixImage(imageA);

            //Właściwe potęgowanie: i - liczba kanałów, j - szerokość, k - wysokość
            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        resultImage.ImageMatrix[i][j, k] = (byte)Math.Min(255, Math.Max(0, (Math.Pow(imageA.ImageMatrix[i][j, k], param))));
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)Math.Pow(imageA.ImageMatrix[i][j, k], param);
                    }

            return resultImage;
        }


        /*
        * Funkcja dzieląca 2 obrazy przez siebie.
        * 
        * @params: MatrixImage imageA, imageB - wczytane obrazy
        */
        public static MatrixImage Division(MatrixImage imageA, MatrixImage imageB)
        {
            //Sprawdzenie czy obrazy mają taką samą skalę barw.
            if (!TheSameBitCount(imageA, imageB))
                return null;

            //Jeśli obrazy mają różne wymiary - ujednolicamy
            if (!TheSameSize(imageA, imageB))
                RasterUniform(ref imageA, ref imageB);

            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            MatrixImage resultImage = new MatrixImage(imageA);

            for (int j = 0; j < imageA.Height; ++j) 
                for (int k = 0; k < imageA.Width; ++k)
                {
                    int max = 0;

                    for (int i = 0; i < imageA.Channels; ++i)
                        max = Math.Max(max, imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k]);

                    if (max == 0)
                        max = 1;

                    for (int i = 0; i < imageA.Channels; ++i)
                    {
                        resultImage.ImageMatrix[i][j, k] = (byte)Math.Ceiling((imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k]) * 255.0 / max);
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)Math.Ceiling((imageA.ImageMatrix[i][j, k] + imageB.ImageMatrix[i][j, k]) * 255.0 / max);
                    }
                }

            return resultImage;
        }

        /*
         * Funkcja dzielaca obraz przez stałą.
         * 
         * @params: MatrixImage imageA - wczytany obraz, int constant - stała
         */
        public static MatrixImage Division(MatrixImage imageA, double constant)
        {
            return Multiplication(imageA, 1.0 / constant);
        }

        /*
       * Funkcja pierwiastkująca obraz.
       * 
       * @params: MatrixImage imageA - wczytany obraz, double param - stopień pierwiastka
       */
        public static MatrixImage RootExtraction(MatrixImage imageA, double param)
        {
            return Exponentiation(imageA, 1.0 / param);
        }

       /*
        * Funkcja logarytmująca obraz.
        * 
        * @params: MatrixImage imageA - wczytany obraz.
        */
        public static MatrixImage Logarithm(MatrixImage imageA, double multiplication)
        {
            //Tworzenie obiektu obrazu wynikowego.
            //Kopiujemy wszystkie dane z obrazu wczytanego, tak by wszystkie struktury były spójne
            //(wywołanie konstruktora kopiującego)
            MatrixImage resultImage = new MatrixImage(imageA);

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        resultImage.ImageMatrixNotNormalized[i][j, k] = (byte)(multiplication * (Math.Log((double)imageA.ImageMatrix[i][j, k] + 1.0)));
                        resultImage.ImageMatrix[i][j, k] = (byte)Math.Min(255, Math.Max(0, (multiplication * (Math.Log((double)imageA.ImageMatrix[i][j, k] + 1.0)))));
                    }

            return resultImage;
        }

        //----------------------ZADANIE 4----------------------//

        /*
        * Funkcja przesuwająca obraz o dany wektor.
        * 
        * @params: MatrixImage imageA- wczytany obraz, int vectorX, int vectorY - parametry przesuniecia
        */
        public static MatrixImage Shifting(MatrixImage imageA, int vectorX, int vectorY)
        {       
            MatrixImage resultImage = new MatrixImage(imageA);

            int previousWidth = imageA.Width;
            int previousHeight = imageA.Height;

            resultImage.ResizeMatrix(resultImage.Width + Math.Abs(vectorX), resultImage.Height + Math.Abs(vectorY));
            resultImage.Clean();

            int startX = vectorX > 0 ? vectorX : 0;
            int startY = vectorY > 0 ? vectorY : 0;
            int endX = startX + previousWidth;
            int endY = startY + previousHeight;

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = startY; j < endY; ++j)
                    for (int k = startX; k < endX; ++k)
                        resultImage.ImageMatrix[i][j, k] = imageA.ImageMatrix[i][j - startY, k - startX];

            return resultImage;
        }

        /*
        * Funkcja skalująca obraz o dany wektor.
        * 
        * @params: MatrixImage imageA- wczytany obraz, double vectorX, double vectorY. ////
        */
        public static MatrixImage Graduation(MatrixImage imageA, double vectorX, double vectorY)
        {
            MatrixImage resultImage = new MatrixImage(imageA);

            resultImage.ResizeMatrix((int)Math.Floor(imageA.Width * vectorX), (int)Math.Floor(imageA.Height * vectorY));

            for (int i = 0; i < resultImage.Channels; ++i)
                for (int j = 0; j < resultImage.Height; ++j)
                    for (int k = 0; k < resultImage.Width; ++k)
                        resultImage.ImageMatrix[i][j, k] = imageA.ImageMatrix[i][(int)Math.Floor(j / vectorY), (int)Math.Floor(k / vectorX)];

            return resultImage;
        }

        /*
        * Funkcja obracająca obraz o zadany kąt od 0 do 90 stopni.
        * 
        * @params: MatrixImage imageA- wczytany obraz, double angle - kąt w stopniach
        */
        public static MatrixImage Rotation0To90(MatrixImage imageA, double angle)
        {
            MatrixImage resultImage = new MatrixImage(imageA);
            angle %= 91.0;
            double radian = angle / 180.0 * Math.PI;

            resultImage.ResizeMatrix(
                (int)Math.Ceiling(
                    Math.Abs(imageA.Width * Math.Cos(radian)) + 
                    Math.Abs(imageA.Height * Math.Sin(radian))
                ), 
                (int)Math.Ceiling(
                    Math.Abs(imageA.Width * Math.Sin(radian)) +
                    Math.Abs(imageA.Height * Math.Cos(radian))
                )
            );

            resultImage.Clean();

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                    {
                        resultImage.ImageMatrix[i][
                                (int)Math.Floor(
                                    j * Math.Cos(radian) + k * Math.Sin(radian)
                                ),
                                (int)Math.Floor(
                                    k * Math.Cos(radian) + (imageA.Height - 1) * Math.Sin(radian) - j * Math.Sin(radian)
                                )
                            ] = imageA.ImageMatrix[i][j, k];
                    }

            return resultImage;
        }

        /*
        * Funkcja obracająca obraz o dowolny kąt
        * 
        * @params: MatrixImage imageA- wczytany obraz, double angle - kąt w stopniach
        */
        public static MatrixImage Rotation(MatrixImage imageA, double angle)
        {
            MatrixImage resultImage = new MatrixImage(imageA);

            angle %= 360.0;
            int squares = (int)Math.Floor(angle / 90);

            for (int i = 0; i < squares; ++i, angle -= 90)
                resultImage = Rotation0To90(resultImage, 90);

            resultImage = Rotation0To90(resultImage, angle);

            return Interpolate(resultImage);
        }

        /*
        * Funkcja obracająca obraz względem osi X
        * 
        * @params: MatrixImage imageA - wczytany obraz
        */
        public static MatrixImage SymetryX(MatrixImage imageA)
        {
            MatrixImage resultImage = new MatrixImage(imageA);

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                        resultImage.ImageMatrix[i][j, k] = imageA.ImageMatrix[i][imageA.Height - j - 1, k];

            return resultImage;
        }

        /*
        * Funkcja obracająca obraz względem osi Y
        * 
        * @params: MatrixImage imageA - wczytany obraz
        */
        public static MatrixImage SymetryY(MatrixImage imageA)
        {
            MatrixImage resultImage = new MatrixImage(imageA);

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = 0; j < imageA.Height; ++j)
                    for (int k = 0; k < imageA.Width; ++k)
                        resultImage.ImageMatrix[i][j, k] = imageA.ImageMatrix[i][j, imageA.Width - k - 1];

            return resultImage;
        }

        /*
        * Funkcja tworząca symetrię obrazu względem prostej 
        * 
        * @params: 
        *   MatrixImage imageA - wczytany obraz 
        *   X1, X2, Y1, Y2 - współrzędne prostej
        */
        public static MatrixImage LineSymetry(MatrixImage imageA, int X1, int Y1, int X2, int Y2)
        {
            MatrixImage resultImage = new MatrixImage(imageA);

            double angle = Math.Atan((double)(X2 - X1) / (double)(Y2 - Y1));
            angle = angle * 180.0 / Math.PI;

            if (angle < 0.0)
            {
                resultImage = Rotation(resultImage, 360 - Math.Abs(angle * 2));
                resultImage = SymetryY(resultImage);
            }
            else
            {
                resultImage = SymetryY(resultImage);
                resultImage = Rotation(resultImage, 360 - Math.Abs(angle * 2));
            }

            return resultImage;
        }

        /*
        * Funkcja wycinająca fragment obrazu
        * 
        * @params: MatrixImage imageA - wczytany obraz, X1, X2, Y1, Y2 - współrzędne prostokąta wycięcia
        */
        public static MatrixImage Cut(MatrixImage imageA, int X1, int Y1, int X2, int Y2)
        {
            MatrixImage resultImage = new MatrixImage(imageA);

            if (X1 > X2)
            {
                int tmp = X1;
                X1 = X2;
                X2 = tmp;
            }

            if (Y1 > Y2)
            {
                int tmp = Y1;
                Y1 = Y2;
                Y2 = tmp;
            }

            if (X1 < 0)
                X1 = 0;
            if (Y1 < 0)
                Y1 = 0;
            if (X2 > imageA.Width - 1)
                X2 = imageA.Width - 1;
            if (Y2 > imageA.Height - 1)
                Y2 = imageA.Height - 1;

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = Y1; j <= Y2; ++j)
                    for (int k = X1; k <= X2; ++k)
                        resultImage.ImageMatrix[i][j, k] = 0;

            return resultImage;
        }

        /*
        * Funkcja kopiująca fragment obrazu
        * 
        * @params: 
        *   MatrixImage imageA - wczytany obraz 
        *   X1, X2, Y1, Y2 - współrzędne prostokąta do skopiowania
        *   X3, Y3 - współrzędne punktu do wklejenia
        */
        public static MatrixImage CopyAndPaste(MatrixImage imageA, int X1, int Y1, int X2, int Y2, int X3, int Y3)
        {
            MatrixImage resultImage = new MatrixImage(imageA);

            if (X1 > X2)
            {
                int tmp = X1;
                X1 = X2;
                X2 = tmp;
            }

            if (Y1 > Y2)
            {
                int tmp = Y1;
                Y1 = Y2;
                Y2 = tmp;
            }

            if (X1 < 0)
                X1 = 0;
            if (Y1 < 0)
                Y1 = 0;
            if (X2 > imageA.Width - 1)
                X2 = imageA.Width - 1;
            if (Y2 > imageA.Height - 1)
                Y2 = imageA.Height - 1;

            int shiftingX = 0, shiftingY = 0;

            if (X3 + (X2 - X1) > resultImage.Width)
                shiftingX = X3 + (X2 - X1) - imageA.Width + 1;
            if (Y3 + (Y2 - Y1) > resultImage.Height)
                shiftingY = Y3 + (Y2 - Y1) - imageA.Height + 1;
            if (X3 < 0)
            {
                shiftingX = X3 - 1;
                X3 = 0;
            }
            if (Y3 < 0)
            {
                shiftingY = Y3 - 1;
                Y3 = 0;
            }

            if (shiftingX != 0 || shiftingY != 0)
                resultImage = Shifting(resultImage, -shiftingX, -shiftingY);

            for (int i = 0; i < imageA.Channels; ++i)
                for (int j = Y1, j2 = Y3; j <= Y2; ++j, ++j2)
                    for (int k = X1, k2 = X3; k <= X2; ++k, ++k2)
                        resultImage.ImageMatrix[i][j2, k2] = imageA.ImageMatrix[i][j, k];

            return resultImage;
        }
    }
}
